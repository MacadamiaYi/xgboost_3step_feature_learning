# xgboost_3Step_Feature_Learning

XGBoost-based 3-step feature learning is a combined embedded-wrapper method for feature selection, which integrates the feature importance ranking under various metrics, NSGA-II based metric selection and subset identification via recursive feature elimination. The method is targeting for supervised settings, and is testing on the [NGSIM dataset](https://data.transportation.gov/Automobiles/Next-Generation-Simulation-NGSIM-Vehicle-Trajector/8ect-6jqj) for lane change intent prediction.

For more details, please find from paper: "A XGBoost-based lane change prediction on time
series data using feature engineering for Autopilot vehicles", IEEE Transactions on Intelligent Transportation Systems (T-ITS), and please use the cite below:

```sh
Y. Zhang, X. Shi, S. Zhang and A. Abraham, "A XGBoost-Based Lane Change Prediction on Time Series Data Using Feature Engineering for Autopilot Vehicles," in IEEE Transactions on Intelligent Transportation Systems, doi: 10.1109/TITS.2022.3170628.
```

## Repository structure

Folder "1_NGSIM_Data_Cleaning_brief_Intro" gives a brief introduction on NGSIM dataset cleaning.

Folder "2_3Step_FeatureLearning_brief_Intro" provides the code for XGBoost-based 3-step feature learning algorithm.

Folder "experiment" provides the detailed codes for experiment in Section V D of paper "A XGBoost-based lane change prediction on time series data using feature engineering for Autopilot vehicles".
